package log

import (
	"github.com/gin-gonic/gin"
	"os"

	"github.com/sirupsen/logrus"
	. "gitlab.com/cloudany-go/microcore/common"
)

func init() {
	if l, e := logrus.ParseLevel(os.Getenv("LOG_LEVEL")); e == nil {
		logrus.SetLevel(l)
	}
	if os.Getenv("LOG_FORMAT") == "json" {
		logrus.SetFormatter(&logrus.JSONFormatter{
			TimestampFormat:  "",
			DisableTimestamp: false,
			DataKey:          "",
			FieldMap:         nil,
			CallerPrettyfier: nil,
			PrettyPrint:      false,
		})
	}
	logrus.SetOutput(os.Stdout)
}

func Tag(tag string) *logrus.Entry {
	return logrus.WithField("tag", tag)
}

func TagWithGinCtx(tag string, ctx *gin.Context) *logrus.Entry {
	l := logrus.WithField("tag", tag)
	if requestID := ctx.GetString(HeaderXRequestID); requestID != "" {
		l = l.WithField(HeaderXRequestID, requestID)
	}
	return l
}

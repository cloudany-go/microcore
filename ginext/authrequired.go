package ginext

import (
	"net/http"

	"github.com/gin-gonic/gin"
	. "gitlab.com/cloudany-go/microcore/common"
)

func AuthRequired(c *gin.Context) {
	headers := struct {
		UserID   int64  `header:"x-user-id" validate:"required,min=1"`
		UserType string `header:"x-user-type"`
	}{}
	if c.ShouldBindHeader(&headers) != nil {
		_ = c.Error(NewError(http.StatusUnauthorized, "unauthorized"))
		c.Abort()
		return
	}

	c.Set(HeaderUserID, headers.UserID)
	c.Set(HeaderUserType, headers.UserType)

	c.Next()
}

type Int64Getter interface {
	GetInt64(key string) int64
}

func GetUserID(c Int64Getter) int64 {
	return c.GetInt64(HeaderUserID)
}

package common

const (
	HeaderXRequestID = "x-request-id"
	HeaderUserID     = "x-user-id"
	HeaderUserType   = "x-user-type"
)

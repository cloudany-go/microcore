module gitlab.com/cloudany-go/microcore

go 1.15

require (
	github.com/caarlos0/env/v6 v6.4.0
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.2.0
	github.com/google/uuid v1.1.2
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.6.1
	gorm.io/driver/postgres v1.0.5
	gorm.io/driver/sqlite v1.1.3
	gorm.io/gorm v1.20.6
)
